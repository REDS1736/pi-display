from PIL import Image, ImageDraw

import module.gui_objects as go


class Canvas:
    _canvas_image: Image
    canvas: ImageDraw

    def __init__(self):
        self._canvas_image = Image.new(
            'RGB',
            (240, 135),
            (255, 255, 255),
        )
        self.canvas = ImageDraw.Draw(self._canvas_image)

    def get_canvas_image(self):
        return self._canvas_image

    def draw(
            self,
            obj: go.GuiObject,
    ) -> None:
        if isinstance(obj, go.GuiText):
            self._canvas_image.paste(
                obj.render_to_image(),
                obj.get_xy(),
                obj.render_to_image(),
            )
        else:
            print('draw not implemented for this type:')
            print(type(obj))

    def fill(
            self,
            color_rgb: tuple[int, int, int],
    ) -> None:
        self.canvas.rectangle(
            xy=(0, 0, 240, 135),
            fill=color_rgb,
        )
