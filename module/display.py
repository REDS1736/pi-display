import board
import time
import digitalio
import displayio
from adafruit_rgb_display import st7789

from module.canvas import Canvas
from module.gui_objects import GuiObject


class Display:
    _backlight: digitalio.DigitalInOut
    _button_a: digitalio.DigitalInOut
    _button_b: digitalio.DigitalInOut
    _canvas: Canvas
    _display: st7789.ST7789
    _last_update: float
    _gui_objects: dict[int, dict[str, GuiObject]]

    def __init__(self):
        displayio.release_displays()
        self._backlight = digitalio.DigitalInOut(board.D22)
        self._backlight.switch_to_output()
        self._button_a = digitalio.DigitalInOut(board.D23)
        self._button_a.switch_to_input()
        self._button_b = digitalio.DigitalInOut(board.D24)
        self._button_b.switch_to_input()
        self._display = st7789.ST7789(
            board.SPI(),
            cs=digitalio.DigitalInOut(board.CE0),
            dc=digitalio.DigitalInOut(board.D25),
            rst=None,
            baudrate=64_000_000,
            width=135,
            height=240,
            x_offset=53,
            y_offset=40,
            rotation=270,
        )
        self._canvas = Canvas()
        self._last_update = None
        self._gui_objects = dict()
        self.apply_canvas(self._canvas)

    def apply_canvas(self, canvas: Canvas) -> None:
        self._display.image(canvas.get_canvas_image())

    def add_gui_object(
            self,
            obj: GuiObject,
            obj_id: str,
            layer: int,
    ) -> None:
        if layer not in self._gui_objects.keys():
            self._gui_objects[layer] = dict()
        if obj_id in self._gui_objects[layer].keys():
            raise Exception()
        self._gui_objects[layer][obj_id] = obj

    def update(self) -> None:
        self.wipe()
        framedelta = 0.0
        if self._last_update is None:
            self._last_update = time.time()
        else:
            framedelta = time.time() - self._last_update
            self._last_update = time.time()
        for layer in sorted(self._gui_objects.keys()):
            layer_objects = self._gui_objects[layer]
            for obj_id, obj in layer_objects.items():
                obj.update(framedelta)
                self._canvas.draw(obj)
        self.apply_canvas(self._canvas)

    def wipe(self) -> None:
        self._canvas.fill((255, 255, 255))

    def reset(self) -> None:
        self._canvas = Canvas()
        self._canvas.fill((255, 255, 255))
        self.apply_canvas(self._canvas)

    def is_button_a_down(self):
        return not self._button_a.value

    def is_button_b_down(self):
        return not self._button_b.value

    def turn_backlight_on(self):
        self._backlight.value = True

    def turn_backlight_off(self):
        self._backlight.value = False
