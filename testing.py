import time

from module.display import Display
import module.gui_objects as go


def main():
    d = Display()
    d.turn_backlight_on()
    test_text = go.GuiText(
        text='Testeditest',
        font_size=15,
        xy=(10, 10),
        color_rgb=(0, 200, 200),
        width=60,
        move_overflow=True,
    )
    d.add_gui_object(
        obj=test_text,
        obj_id='test_text',
        layer=1,
    )
    d.update()
    next_text = go.GuiText(
        text='Noch ein Test',
        font_size=15,
        xy=(10, 30),
        color_rgb=(200, 0, 200),
        width=200,
    )
    d.add_gui_object(
        obj=next_text,
        obj_id='next_text',
        layer=1
    )
    d.update()
    next_text.set_text('Neuer Text')
    d.update()
    input('loop?...')
    while True:
        if d.is_button_a_down():
            break
        next_text.set_text(str(time.time()))
        d.update()
    print('done!')
    d.reset()
    d.turn_backlight_off()


if __name__ == '__main__':
    main()
