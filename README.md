# Prerequisites

First, some pip packages:

```
python -m pip install adafruit-circuitpython-rgb-display adafruit-circuitpython-st7789
python -m pip install ---upgrade --force-reinstall spidev
```


## Das hier ist glaub ich überflüssig:

Then, CircuitPython:

```
python -m pip install --upgrade adafruit-python-shell
wget https://raw.githubusercontent.com/adafruit/Raspberry-Pi-Installer-Scripts/master/raspi-blinka.py
sudo python raspi-blinka.py
```

The install script will ask you to reboot your system.
Do it.

